package com.estimote.examples.demos;

import java.util.ArrayList;

import org.apache.http.util.LangUtils;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.Menu;

public class LaunchPlaylistActivity extends Activity {
	private static final String TAG = LaunchPlaylistActivity.class
			.getSimpleName();
	String playlistid = "";

	// Cursor cursor = getContentResolver().query
	// (MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, null, null, null,
	// null);
	// if (cursor != null) {
	// Log.i(TAG,"Cursor not null");
	// if (cursor.moveToFirst()) {
	// do {
	// Log.i(TAG,"Entered");
	// playlistid =
	// cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Playlists._ID));
	// playListIDS.add(playlistid);
	// } while (cursor.moveToNext());
	// cursor.close();
	// }
	// Log.i(TAG,"playlistid: "+playListIDS.size());
	// }
	//
	//
	// Intent intent = new Intent(Intent.ACTION_VIEW);
	// intent.setComponent(new ComponentName
	// ("com.android.music","com.android.music.PlaylistBrowserActivity"));
	// intent.setType(MediaStore.Audio.Playlists.CONTENT_TYPE);
	// intent.setFlags(0x10000000);
	// intent.putExtra("oneshot", false);
	// intent.putExtra("playlist", playListIDS.get(0));
	// startActivity(intent);
	// ArrayList<String> playListIDS = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_launch_playlist);

		final Uri exAudioUri = Uri
				.parse("content://com.google.android.music.MusicContent/playlists");

		final String[] proj = { MediaStore.Audio.Playlists._ID };

		final Cursor musiccursor = getContentResolver().query(exAudioUri, proj,
				null, null, null);

		String id = null;

		if (musiccursor != null && musiccursor.getCount() > 0) {
			while (musiccursor.moveToNext()) {
				id = musiccursor.getString(musiccursor
						.getColumnIndexOrThrow(MediaStore.Audio.Playlists._ID));

			}

		}
		Intent playMediaIntent = new Intent(Intent.ACTION_VIEW);
		playMediaIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		playMediaIntent.setType(MediaStore.Audio.Playlists.CONTENT_TYPE);

		playMediaIntent.putExtra("playlist", id);
		Log.i(TAG, "id: " + id);
		startActivity(playMediaIntent);
	}

}

//package com.estimote.examples.demos;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.http.util.LangUtils;
//
//import com.estimote.sdk.Beacon;
//import com.estimote.sdk.BeaconManager;
//import com.estimote.sdk.Region;
//
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.RemoteException;
//import android.provider.MediaStore;
//import android.app.Activity;
//import android.app.NotificationManager;
//import android.bluetooth.BluetoothAdapter;
//import android.content.ComponentName;
//import android.content.Context;
//import android.content.Intent;
//import android.database.Cursor;
//import android.util.Log;
//import android.view.Menu;
//import android.widget.Toast;
//
//public class LaunchPlaylistActivity extends Activity {
//	private static final String TAG = LaunchPlaylistActivity.class
//			.getSimpleName();
//	String playlistid = "";
//	private BeaconManager beaconManager;
//	private NotificationManager notificationManager;
//	
//	private static final String ESTIMOTE_PROXIMITY_UUID = "SPEED-LIMIT-REGION";
//	private static final int REQUEST_ENABLE_BT = 1234;
//	private static final Region SPEED_BEACON_REGION = new Region("rid",
//			ESTIMOTE_PROXIMITY_UUID, null, null);
//
//	// Cursor cursor = getContentResolver().query
//	// (MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, null, null, null,
//	// null);
//	// if (cursor != null) {
//	// Log.i(TAG,"Cursor not null");
//	// if (cursor.moveToFirst()) {
//	// do {
//	// Log.i(TAG,"Entered");
//	// playlistid =
//	// cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Playlists._ID));
//	// playListIDS.add(playlistid);
//	// } while (cursor.moveToNext());
//	// cursor.close();
//	// }
//	// Log.i(TAG,"playlistid: "+playListIDS.size());
//	// }
//	//
//	//
//	// Intent intent = new Intent(Intent.ACTION_VIEW);
//	// intent.setComponent(new ComponentName
//	// ("com.android.music","com.android.music.PlaylistBrowserActivity"));
//	// intent.setType(MediaStore.Audio.Playlists.CONTENT_TYPE);
//	// intent.setFlags(0x10000000);
//	// intent.putExtra("oneshot", false);
//	// intent.putExtra("playlist", playListIDS.get(0));
//	// startActivity(intent);
//	// ArrayList<String> playListIDS = new ArrayList<String>();
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_launch_playlist);
//		
//		final Uri exAudioUri = Uri
//				.parse("content://com.google.android.music.MusicContent/playlists");
//
//		final String[] proj = { MediaStore.Audio.Playlists._ID };
//
//		final Cursor musiccursor = getContentResolver().query(exAudioUri, proj,
//				null, null, null);
//
//		String id = null;
//
//		if (musiccursor != null && musiccursor.getCount() > 0) {
//			while (musiccursor.moveToNext()) {
//				id = musiccursor.getString(musiccursor
//						.getColumnIndexOrThrow(MediaStore.Audio.Playlists._ID));
//
//			}
//
//		}
//		Intent playMediaIntent = new Intent(Intent.ACTION_VIEW);
//		playMediaIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		playMediaIntent.setType(MediaStore.Audio.Playlists.CONTENT_TYPE);
//
//		playMediaIntent.putExtra("playlist", id);
//		Log.i(TAG, "id: " + id);
//		startActivity(playMediaIntent);
////		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
////		beaconManager = new BeaconManager(this);
////		beaconManager.setRangingListener(new BeaconManager.RangingListener() {
////			@Override
////			public void onBeaconsDiscovered(Region region,
////					final List<Beacon> beacons) {
////				// Note that results are not delivered on UI thread.
////				runOnUiThread(new Runnable() {
////					@Override
////					public void run() {
////
////						for (Beacon b : beacons) {
////							if (b.getMacAddress().equals("")) {
////								//intent activity fabio
////								beaconManager.disconnect();
////							}
////							
////						}
////					}
////				});
////			}
////		});
////		
//	}
//
//	@Override
//	protected void onStart() {
//		// TODO Auto-generated method stub
//		super.onStart();
//		if (!beaconManager.hasBluetooth()) {
//			Toast.makeText(this, "Device does not have Bluetooth Low Energy",
//					Toast.LENGTH_LONG).show();
//			return;
//		}
//
//		// If Bluetooth is not enabled, let user enable it.
//		if (!beaconManager.isBluetoothEnabled()) {
//			Intent enableBtIntent = new Intent(
//					BluetoothAdapter.ACTION_REQUEST_ENABLE);
//			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
//		} else {
//			connectToService();
//		}
//	}
//	@Override
//	protected void onPause() {
//		// TODO Auto-generated method stub
//		super.onPause();
//		Toast.makeText(LaunchPlaylistActivity.this, "Inside onPause()", Toast.LENGTH_SHORT).show();
//	}
//	@Override
//	protected void onStop() {
////		try {
////			beaconManager.stopRanging(SPEED_BEACON_REGION);
////		} catch (RemoteException e) {
////			Log.d(TAG, "Error while stopping ranging", e);
////		}
//
//		super.onStop();
//	}
//	
//	@Override
//	protected void onDestroy() {
//		// TODO Auto-generated method stub
//		super.onDestroy();
//		Toast.makeText(LaunchPlaylistActivity.this, "Inside onDestroy()", Toast.LENGTH_SHORT).show();
//	}
//	
//	private void connectToService() {
////		beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
////			@Override
////			public void onServiceReady() {
////				try {
////					beaconManager.startRanging(SPEED_BEACON_REGION);
////				} catch (RemoteException e) {
////					Toast.makeText(
////							LaunchPlaylistActivity.this,
////							"Cannot start ranging, something terrible happened",
////							Toast.LENGTH_LONG).show();
////					Log.e(TAG, "Cannot start ranging", e);
////				}
////			}
////		});
//
//	}
//}
