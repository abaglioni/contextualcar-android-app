package com.estimote.examples.demos;

import java.io.IOException;
import java.util.List;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Shows all available demos.
 * 
 * @author wiktor@estimote.com (Wiktor Gworek)
 */
public class MainActivity extends Activity {

	private static final String MONICA_BEACON = "C4:BC:6F:E9:7E:C5";
	private static final String ALERT_BEACON = "E2:8B:61:B7:59:32";
	private NotificationManager notificationManager;
	private static final int NOTIFICATION_ID = 123;
	private static final int REQUEST_ENABLE_BT = 1234;
	private static final String ESTIMOTE_PROXIMITY_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
	private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid",
			ESTIMOTE_PROXIMITY_UUID, null, null);
	private static final String TAG = MainActivity.class.getSimpleName();
	private BeaconManager beaconManager;
	private AlertManager alertManager;
	private boolean alertBeaconFound = false;
	private boolean monicaBeaconFound = false;
	private int timeoutAlert = 0;
	private boolean notificationThereAlready = false;
	private int weightedBeaconDistance = -150;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.all_demos);
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		//Context context = this.getBaseContext();
		alertManager = new AlertManager(this);
		beaconManager = new BeaconManager(this);
		beaconManager.setRangingListener(new BeaconManager.RangingListener() {
			@Override
			public void onBeaconsDiscovered(Region region,
					final List<Beacon> beacons) {
				
				// Note that results are not delivered on UI thread.
				runOnUiThread(new Runnable() {
					
					private Beacon monicaBeacon;

					@Override
					public void run() {
						ImageView logo = (ImageView) findViewById(R.id.imageView1);
						alertBeaconFound = false;
						monicaBeaconFound = false;
						for (Beacon b : beacons) {
							if (b.getMacAddress().equals(ALERT_BEACON)) {
								Log.d(TAG, "alert beacon found");
								alertBeaconFound = true;
							} else {

								if (isMonicaBeaconNear(b)) {
									monicaBeaconFound = true;
									monicaBeacon=b;
								}
							}
						}
						if (alertBeaconFound) {
						    alertManager.RaiseAlert(logo);
							//raiseDangerAlert(logo);
						} else {
							alertManager.removeAlert(logo);
							timeoutAlert++;
							Log.d(TAG, "timeout" + timeoutAlert);
							if (timeoutAlert > 10) {
								Log.d(TAG, "placing Fiat logo");
								timeoutAlert = 11;
								alertManager.removeAlert(logo);
								
							}
						}
						if (monicaBeaconFound) {
							if (!notificationIsThereAlready()) {
								postNotification("Hi Monica, launch your best playlist!");
								notificationThereAlready = true;
							}

						} else {
							removeNotification();
							notificationThereAlready = false;
						}
					}

					private void raiseDangerAlert(ImageView logo) {
						//ImageView logo = (ImageView) findViewById(R.id.imageView1);
						logo.setImageResource(R.drawable.danger);
						MediaPlayer mPlayer = MediaPlayer.create(MainActivity.this, R.raw.carhorn);
						try {
							mPlayer.start();
						} catch (IllegalStateException e) {
							e.printStackTrace();
						} 
						
					}

					private boolean isMonicaBeaconNear(Beacon b) {
						if (b.getMacAddress().equals(MONICA_BEACON))
						return Utils.computeAccuracy(b)<1;
						else return false;
						
					}

					private void removeNotification() {
						Log.d(TAG, "rimossa!");
						notificationManager.cancel(NOTIFICATION_ID);

					}

					private boolean notificationIsThereAlready() {
						return notificationThereAlready;
					}
				});
			}
		});
		
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (!beaconManager.hasBluetooth()) {
			Toast.makeText(this, "Device does not have Bluetooth Low Energy",
					Toast.LENGTH_LONG).show();
			return;
		}

		// If Bluetooth is not enabled, let user enable it.
		if (!beaconManager.isBluetoothEnabled()) {
			Intent enableBtIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		} else {
			connectToService();
		}
	}

	@Override
	protected void onStop() {
		try {

			beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);
		} catch (RemoteException e) {
			Log.d(TAG, "Error while stopping ranging", e);
		}

		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		notificationManager.cancel(NOTIFICATION_ID);
	}

	private void connectToService() {
		beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
			@Override
			public void onServiceReady() {
				try {
					beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
				} catch (RemoteException e) {
					Toast.makeText(
							MainActivity.this,
							"Cannot start ranging, something terrible happened",
							Toast.LENGTH_LONG).show();
					Log.e(TAG, "Cannot start ranging", e);
				}
			}
		});

	}

	private void postNotification(String msg) {

		Intent playMediaIntent = createPlayIntent();

		PendingIntent pendingIntent = PendingIntent.getActivities(
				MainActivity.this, 0, new Intent[] { playMediaIntent },
				PendingIntent.FLAG_ONE_SHOT);
		Notification notification = new Notification.Builder(MainActivity.this)
				.setSmallIcon(R.drawable.beacon_blue)
				.setContentTitle("Contextual Car Music").setContentText(msg)
				.setAutoCancel(false).setContentIntent(pendingIntent).build();
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_LIGHTS;
		notificationManager.notify(NOTIFICATION_ID, notification);

	}

	private Intent createPlayIntent() {
		final Uri exAudioUri = Uri
				.parse("content://com.google.android.music.MusicContent/playlists");

		final String[] proj = { MediaStore.Audio.Playlists._ID };

		final Cursor musiccursor = getContentResolver().query(exAudioUri, proj,
				null, null, null);

		String id = null;

		if (musiccursor != null && musiccursor.getCount() > 0) {
			while (musiccursor.moveToNext()) {
				id = musiccursor.getString(musiccursor
						.getColumnIndexOrThrow(MediaStore.Audio.Playlists._ID));

			}

		}
		Intent playMediaIntent = new Intent(Intent.ACTION_VIEW);
		playMediaIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		playMediaIntent.setType(MediaStore.Audio.Playlists.CONTENT_TYPE);

		playMediaIntent.putExtra("playlist", id);
		Log.i(TAG, "id: " + id);
		return playMediaIntent;
	}
	

}
