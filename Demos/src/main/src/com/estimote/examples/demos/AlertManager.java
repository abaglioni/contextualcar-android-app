package com.estimote.examples.demos;

import android.media.MediaPlayer;
import android.widget.ImageView;

public class AlertManager {
	
	

	private MainActivity activity;
	private boolean alreadyRaisedAlert=false;

	public AlertManager(MainActivity mainActivity) {
		this.activity=mainActivity;
	}

	public void RaiseAlert(ImageView logo) {
		if (!alreadyRaisedAlert){
		logo.setImageResource(R.drawable.danger);
		MediaPlayer mPlayer = MediaPlayer.create(activity, R.raw.carhorn);
		try {
			mPlayer.start();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} 
		alreadyRaisedAlert=true;
		}
	}

	public void removeAlert(ImageView logo) {
		logo.setImageResource(R.drawable.fiat_logo);
		alreadyRaisedAlert=false;
	}

}
